var fs = require('fs');
var path = require('path');
var buf;
var dir = process.argv[2];
var filterStr = process.argv[3];

module.exports = function (dir, filterStr, call){

    
        fs.readdir(dir, function(err, data){
            if (err) 
                return call(err);

            data = data.filter(function (file){
                return path.extname(file) === '.' + filterStr
            });

            call(null, data);
        });
        
    
}

