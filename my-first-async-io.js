var fs = require('fs');
var buf = undefined;

function readFile(call){
    fs.readFile(process.argv[2], function(err, data){
        if (err) console.log(err);
        buf = data;
        call();
    });
    
}

function logThis(){
    var str = buf.toString();

    var lines = str.split("\n");

    console.log(lines.length -1);
}

readFile(logThis);



