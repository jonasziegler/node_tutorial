var net = require('net');

function addZeros(i){
    if (i < 10){
        return "0" +i;
    }else{
        return "" +i;
    }
}

function todaysDate(){
    var d = new Date();
    return d.getFullYear()+'-'+ addZeros(d.getMonth()+1)+'-'+ addZeros(d.getDate())+ ' '
    + addZeros(d.getHours()) + ':' + addZeros(d.getMinutes());
}


var server = net.createServer(function (socket){
    socket.end(todaysDate() + '\n');
});

server.listen(process.argv[2]);