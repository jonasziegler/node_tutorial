var fs = require('fs');
var path = require('path');
var buf;
var extention = "."+process.argv[3];

function readFile(call){
    fs.readdir(process.argv[2], function(err, data){
        if (err) console.log(err);
        buf = data;
        call();
    });
    
}

function logThis(){
   var filtered = "";
   for(i=0;i<buf.length;i++){
        if (path.extname(buf[i]) == extention){
            filtered += buf[i] + "\n";
        }
   }
   console.log(filtered);
}

readFile(logThis);

